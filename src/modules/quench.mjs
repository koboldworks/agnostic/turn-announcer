import { wrapNumber } from '../helpers/math.mjs';
/* global quench */
quench.registerBatch(
	'koboldworks-turn-announcer.helpers',
	async function testHelpers({ describe, it, expect, before, after } = {}) {
		describe('wrapNumber', function testWrapNumber() {
			it('wrap 5 [0 – 4] = 0', function () {
				expect(wrapNumber(5, 0, 4)).to.equal(0);
			});
			it('wrap 6 [2 – 4] = 3', function () {
				expect(wrapNumber(6, 2, 4)).to.equal(3);
			});
			it('wrap -1 [0 – 4] = 4', function () {
				expect(wrapNumber(-1, 0, 4)).to.equal(4);
			});
			it('wrap -2 [0 – 4] = 3', function () {
				expect(wrapNumber(-2, 0, 4)).to.equal(3);
			});
			it('wrap -2 [2 – 4] = 4', function () {
				expect(wrapNumber(-2, 2, 4)).to.equal(4);
			});
			it('wrap -102 [0 – 10] = 8', function () {
				expect(wrapNumber(-102, 0, 10)).to.equal(8);
			});
			it('wrap 103 [0 – 10] = 4', function () {
				expect(wrapNumber(103, 0, 10)).to.equal(4);
			});
		});
	},
	{ displayName: game.i18n.localize('Koboldworks.TurnAnnouncer.Tests.Helpers') }
)
