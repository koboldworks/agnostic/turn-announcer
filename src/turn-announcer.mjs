const CFG = {
	id: 'koboldworks-turn-announcer',
	SETTING: {
		missedKey: 'missedTurn',
		hidePortrait: 'hidePortrait',
		obfuscateNPCs: 'obfuscateNPCs',
		roundCycling: 'roundCycling',
		compact: 'compact',
		turnLabel: 'turnLabel',
		roundLabel: 'roundLabel',
	},
};

const getPlayerOwners = (thing, perm) => game.users.players.filter(u => thing.testUserPermission(u, perm));

const escapeHTML = (unsafe) => unsafe.replaceAll('&', '&amp;').replaceAll('<', '&lt;').replaceAll('>', '&gt;').replaceAll('"', '&quot;').replaceAll('\'', '&apos;');

import { wrapNumber } from './helpers/math.mjs';

const turn_template_file = `modules/${CFG.id}/template/turn-announcer.hbs`,
	round_template_file = `modules/${CFG.id}/template/round-cycling.hbs`;

let turnTemplate, roundTemplate;
Hooks.once('init', () => {
	getTemplate(turn_template_file).then(t => turnTemplate = t);
	getTemplate(round_template_file).then(t => roundTemplate = t);
});

const lastCombatant = {
	combatant: null,
	get tokenId() { return this.combatant?.token?.id; },
	spoke: false
};

/**
 * @param {Element} content
 */
function hideContent(content) {
	if (content) {
		content.replaceChildren();
		content.style.display = 'none';
	}
}

/**
 * @param {ChatMessage} cm
 * @param {Element} html
 * @param {Element} main
 */
function compressTurnAnnounceMessage(cm, html, main) {
	const compact = game.settings.get(CFG.id, CFG.SETTING.compact);
	if (compact) main.classList.add('compact');
}

/**
 * @param {ChatMessage} cm
 * @param {Element} html
 * @param {Element} main
 */
function compressRoundCyclingMessage(cm, html, main) {
	if (!main) return;
	main.classList.add('koboldworks', 'round-cycling');

	const header = html.querySelector('.message-header');
	if (!header) return;
	// Remove sender
	header.querySelector('.message-sender')?.remove();

	const content = html.querySelector('.message-content');
	if (!content) return;
	const msg = content.querySelector('.round-message');

	header.prepend(msg);
	const icon = document.createElement('i');
	icon.classList.add('fas', 'fa-hourglass-start');
	msg?.prepend(icon, ' ');

	// Migrate data attributes
	const data = content.querySelector('.round-cycling');
	if (data?.dataset) {
		main.dataset.combatId = data.dataset.combatId;
		main.dataset.round = data.dataset.round;
	}

	// Remove old content
	hideContent(content);
}

/**
 * @param {ChatMessage} cm
 * @param {Element} html
 * @param {Element} main
 */
function compressMissedTurnMessage(cm, html, main) {
	main.classList.add('koboldworks', 'missed-turn-announcer');

	const content = html.querySelector('.message-content > .turn-announcer');

	// Remove things to enforce formatting
	const header = main.querySelector('.message-header');
	if (!header) return;
	header.classList.remove('message-header');
	const wh = header.querySelector('.whisper-to');
	if (wh) {
		main.title = wh.textContent;
		wh.remove();
	}
	const timestamp = header.querySelectorAll('.message-sender,.message-timestamp');
	timestamp?.forEach(el => el.style.display = 'none');

	const msgb = document.createElement('div');
	msgb.classList.add('missed-token');
	const missedIcon = content.querySelector('.missed-turn-icon'),
		missedText = content.querySelector('.missed-turn-text'),
		missedCombatant = content.querySelector('.missed-combatant'),
		missedName = document.createElement('span');
	missedName.classList.add('missed-turn-name');
	missedName.textContent = missedCombatant?.textContent?.trim() ?? '...';
	msgb.append(missedIcon, ' ', missedName, ' ', missedText);
	header.prepend(msgb);

	if (content) hideContent(content);
}

function updateLastCombatantFromMsg(cm, flags) {
	lastCombatant.spoke = false;
	lastCombatant.combatant = game.combat?.getCombatantByToken(flags.token) ?? null;
}

/**
 * @param {ChatMessage} cm
 * @param {JQuery} html
 * @param {object} _options
 */
function chatMessageEvent(cm, [html], _options) {
	const isGM = game.user.isGM;
	const flags = cm.flags?.[CFG.id];

	if (!flags) {
		if (isGM && cm.speaker?.token === lastCombatant.tokenId)
			lastCombatant.spoke = true;
		return;
	}

	const main = html.closest('[data-message-id]');
	html?.classList.add('turn-announcer', 'koboldworks');

	if (flags.missedTurn)
		compressMissedTurnMessage(cm, html, main);
	else if (flags.turnAnnounce || flags.token) {
		updateLastCombatantFromMsg(cm, flags);
		compressTurnAnnounceMessage(cm, html, main);
	}
	else if (flags.roundCycling) {
		compressRoundCyclingMessage(cm, html, main);
	}

	main?.querySelector('.whisper-to')?.remove();

	// De-obfuscate name for GM
	if (cm.getFlag(CFG.id, 'obfuscated')) {
		html.classList.add('obfuscated');

		const combat = game.combats.get(cm.getFlag(CFG.id, 'combat')),
			combatant = combat?.combatants.get(cm.getFlag(CFG.id, 'combatant'));

		if (!combatant?.actor?.testUserPermission(game.user, 'OBSERVER')) return;

		if (combatant?.token) {
			const name = html.querySelector('.actor .name-box .name');
			if (name) name.textContent = combatant.token.name;
		}
	}
}

let lastReported = {};

function missedTurn(data, context) {
	if (data.last?.spoke || data.last == null) return; // They spoke, nothing more to do.

	const preUpdate = context._turnAnnouncer;
	if (preUpdate?.combat === game.combats.active.id) {
		// Rolling back
		if (preUpdate.roundShift < 0 || preUpdate.turnShift < 0 && preUpdate.roundShift <= 0)
			return;
	}

	const { last, combat, combatant } = data;
	if (lastReported.combat !== undefined && combat.id !== lastReported.combat) return;
	if (last.token?.id === combatant?.token?.id) return; // Same character's turn?
	if (lastReported.id === last.token?.id) return;

	lastReported = { id: last.token?.id, combat: combat.id };

	const mt = game.i18n.localize('Koboldworks.TurnAnnouncer.MissedTurn');

	const scene = combat.scene,
		actor = last.actor?.id,
		token = last.token?.id,
		alias = last.name;

	if (actor === undefined || token === undefined) return;

	const cmName = escapeHTML(data.last.combatant.name);

	const msgData = {
		content: `<div class="koboldworks turn-announcer"><span class='missed-turn-icon'><i class="fas fa-exclamation-triangle"></i></span><span class='missed-combatant'>${cmName}</span> <span class='missed-turn-text'>${mt}</span></div>`,
		speaker: { scene, actor, token, alias },
		flags: {
			[CFG.id]: { missedTurn: true, token, actor, combatant: last.combatant.id }
		},
		// type: CONST.CHAT_MESSAGE_TYPES.OOC
	};

	ChatMessage.applyRollMode(msgData, CONST.DICE_ROLL_MODES.PRIVATE);

	return msgData;
}

function generateCards(info, context) {
	const msgs = [];
	if (game.settings.get(CFG.id, CFG.SETTING.missedKey)) {
		const msg = missedTurn(info, context);
		if (msg) msgs.push(msg);
	}

	if (info.combatant.defeated) return msgs; // undesired
	if (info.last?.combatant != null && info.last.combatant.id === info.combatant.id) return msgs; // don't report the same thing multiple times

	const speaker = info.obfuscated ? { user: game.user.id } : ChatMessage.getSpeaker({ token: info.token?.document, actor: info.actor });

	const minPerm = CONST.DOCUMENT_OWNERSHIP_LEVELS.OBSERVER;
	const defaultVisible = info.hidden ? false : info.actor?.ownership?.default >= minPerm;

	const cardData = {
		content: turnTemplate(info, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true }),
		speaker,
		rollMode: defaultVisible ? 'publicroll' : 'gmroll',
		whisper: defaultVisible ? [] : getPlayerOwners(info.actor, minPerm).map(u => u.id),
		flags: {
			[CFG.id]: {
				turnAnnounce: true,
				token: info.token?.id,
				round: info.round,
				turn: info.turn,
				combat: info.combat.id,
				combatant: info.combatant.id,
				obfuscated: info.obfuscated
			}
		},
	};

	ChatMessage.applyRollMode(cardData, !info.hidden ? 'publicroll' : 'gmroll')
	if (cardData.rollMode === 'publicroll') delete cardData.whisper;

	msgs.push(cardData);

	return msgs;
}

function generateRoundCyclingCard(combat) {
	const speaker = ChatMessage.getSpeaker('GM');
	const override = game.settings.get(CFG.id, CFG.SETTING.roundLabel);
	const data = { combat };
	if (override) data.message = override.replace('{round}', combat.round);
	else data.message = game.i18n.format('Koboldworks.TurnAnnouncer.RoundCycling', { round: combat.round });
	const msgData = {
		content: roundTemplate(data, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true }),
		speaker,
		rollMode: 'publicroll',
		flags: {
			[CFG.id]: { roundCycling: true, round: combat.round, combat: combat.id }
		},
	};

	// Spoof GM as message owner
	if (!game.user.isGM) {
		msgData.author = game.users.activeGM?.id ?? game.users.filter(u => u.isGM)[0].id;
	}

	return msgData;
}

/**
 * @param {Combat} combat
 * @param context
 */
function announceRoundCycling(combat, context) {
	// Skip in case turns were rolled back.
	if (combat.__turnAnnouncerLastRoundCycle >= combat.round) return;
	if (context._turnAnnouncer.roundShift <= 0) return;
	combat.__turnAnnouncerLastRoundCycle = combat.round;
	return generateRoundCyclingCard(combat);
}

/**
 * @param {Combat} combat
 * @param {string} userId
 * @param context
 */
function announceTurn(combat, context) {
	// Only continue with first GM in the list
	// if (!game.user.isGM || game.users.filter(o => o.isGM && o.active).sort((a, b) => b.role - a.role)[0].id !== game.user.id) return;

	const combatant = combat.combatant;
	const defeated = combatant?.defeated ?? false;
	const tokenDoc = combatant?.token;
	if (!tokenDoc) return; // Combatant with no token, unusual, but possible

	const previous = {
		combatant: lastCombatant.combatant, // cache
		get defeated() { return this.combatant?.defeated; },
		get token() { return this.combatant?.token; },
		get actor() { return this.combatant?.actor; },
		spoke: lastCombatant.combatant?.defeated ? false : (lastCombatant?.spoke ?? false), // dead don't speak
	};

	// Update for next cycle
	if (!defeated) {
		lastCombatant.combatant = combatant;
		lastCombatant.spoke = false;
	}

	if (!combat.started) return;

	const info = {
		actor: combatant.actor,
		combatant,
		combat,
		last: previous,
		/** @type {Token|null} */
		token: tokenDoc.object,
		tokenDoc,
		get round() { return this.combat?.round; },
		get turn() { return this.combat?.turn; },
		user: combatant.players[0] ?? game.users.activeGM ?? game.users.find(u => u.isGM),
		get player() { return this.user?.name; },
		name: null,
		label: null,
		get hidden() { return this.combatant?.hidden ?? false; },
		get visible() { return this.combatant?.visible ?? false; },
		obfuscated: false,
	};

	info.name = info.token?.name ?? combatant.name;
	info.portrait = tokenDoc?.texture.src ?? info.actor.prototypeToken?.texture.src;

	const obfuscateType = game.settings.get(CFG.id, CFG.SETTING.obfuscateNPCs);
	const hasVisibleName = () => info.token ? [30, 50].includes(tokenDoc.displayName) : true; // 30=hovered by anyone or 50=always for everyone
	const obfuscate = {
		get all() { return false; },
		get owned() { return !info.actor.hasPlayerOwner; },
		get token() { return !hasVisibleName(); },
		get any() { return !(info.actor?.hasPlayerOwner || hasVisibleName()); }
	};
	info.obfuscated = obfuscate[obfuscateType] ?? false;
	if (info.obfuscated) info.name = game.i18n.localize('Koboldworks.TurnAnnouncer.UnidentifiedTurn');

	const label = `<span class='name'>${escapeHTML(info.name)}</span>`;
	const override = game.settings.get(CFG.id, CFG.SETTING.turnLabel);
	if (override) info.label = override.replace('{name}', label);
	else info.label = game.i18n.format('Koboldworks.TurnAnnouncer.Turn', { name: label });

	if (!info.portrait || game.settings.get(CFG.id, CFG.SETTING.hidePortrait))
		info.hidePortrait = true;

	const msgs = generateCards(info, context);

	return msgs;
}

/**
 * @param {Combat} combat
 * @param {object} changed
 * @param {object} context
 * @param {string} userId
 */
function processTurn(combat, changed, context, userId) {
	if (game.user.id !== userId) return; // Trust the one provoking combat update has sufficient permissions

	const msgs = [];

	// Round cycling message
	if (game.settings.get(CFG.id, CFG.SETTING.roundCycling)) {
		const roundMsg = announceRoundCycling(combat, context);
		if (roundMsg) msgs.push(roundMsg);
	}

	// Turn announcement
	const turnMsgs = announceTurn(combat, context);
	if (turnMsgs?.length) msgs.push(...turnMsgs);

	if (msgs.length) ChatMessage.create(msgs);
}

/* REGISTER SETTINGS */

Hooks.once('init', () => {
	game.settings.register(CFG.id, CFG.SETTING.turnLabel, {
		name: 'Koboldworks.TurnAnnouncer.TurnCardLabel',
		hint: 'Koboldworks.TurnAnnouncer.TurnCardHint',
		type: String,
		config: true,
		scope: 'world',
		default: '',
	});

	game.settings.register(CFG.id, CFG.SETTING.roundLabel, {
		name: 'Koboldworks.TurnAnnouncer.RoundCardLabel',
		hint: 'Koboldworks.TurnAnnouncer.RoundCardHint',
		type: String,
		config: true,
		scope: 'world',
		default: '',
	});

	game.settings.register(CFG.id, CFG.SETTING.obfuscateNPCs, {
		name: 'Koboldworks.TurnAnnouncer.ObfuscateNPCsLabel',
		hint: 'Koboldworks.TurnAnnouncer.ObfuscateNPCsHint',
		type: String,
		config: true,
		scope: 'world',
		choices: {
			all: 'Koboldworks.TurnAnnouncer.ObfuscateNPCsVisibility.All',
			owned: 'Koboldworks.TurnAnnouncer.ObfuscateNPCsVisibility.Owned',
			token: 'Koboldworks.TurnAnnouncer.ObfuscateNPCsVisibility.Token',
			// visible: 'Koboldworks.TurnAnnouncer.ObfuscateNPCsVisibility.Visible',
			any: 'Koboldworks.TurnAnnouncer.ObfuscateNPCsVisibility.Any',
		},
		default: 'all',
	});

	game.settings.register(CFG.id, CFG.SETTING.roundCycling, {
		name: 'Koboldworks.TurnAnnouncer.RoundCyclingLabel',
		hint: 'Koboldworks.TurnAnnouncer.RoundCyclingHint',
		type: Boolean,
		config: true,
		scope: 'world',
		default: true,
	});

	game.settings.register(CFG.id, CFG.SETTING.missedKey, {
		name: 'Koboldworks.TurnAnnouncer.MissedTurnLabel',
		hint: 'Koboldworks.TurnAnnouncer.MissedTurnHint',
		type: Boolean,
		config: true,
		scope: 'world',
		default: false,
		onChange: (value) => value ? Hooks.on('renderChatMessage', chatMessageEvent) : Hooks.off('renderChatMessage', chatMessageEvent)
	});

	game.settings.register(CFG.id, CFG.SETTING.hidePortrait, {
		name: 'Koboldworks.TurnAnnouncer.HidePortraitLabel',
		hint: 'Koboldworks.TurnAnnouncer.HidePortraitHint',
		type: Boolean,
		config: true,
		scope: 'world',
		default: false,
	});

	game.settings.register(CFG.id, CFG.SETTING.compact, {
		name: 'Koboldworks.TurnAnnouncer.CompactLabel',
		hint: 'Koboldworks.TurnAnnouncer.CompactHint',
		type: Boolean,
		config: true,
		scope: 'world',
		default: true,
	});
});

/* HOOKS */

/**
 * Detect what was changed and how and pass it along to the real update.
 */
Hooks.on('preUpdateCombat', function preUpdateCombat(combat, updateData, context) {
	const roundDelta = updateData.round !== undefined ? updateData.round - combat.round : 0,
		turnCount = combat.turns.length,
		roundAdjust = roundDelta * turnCount,
		forward = roundDelta >= 0,
		turnDelta = updateData.turn !== undefined ? updateData.turn - combat.turn : 0,
		// Messy calculation for round changesk
		turnDeltaWrapped = roundDelta == 0 ? turnDelta : wrapNumber(roundAdjust + turnDelta, forward ? 0 : -(turnCount - 1), forward ? turnCount - 1 : 0);

	// TODO: This does not behave nicely if larger time changes happen

	context._turnAnnouncer = {
		turnShift: turnDeltaWrapped,
		roundShift: roundDelta,
		combat: combat.id,
	};
});

// Should the handling be wholly in preUpdate?
Hooks.on('updateCombat', processTurn);
Hooks.on('renderChatMessage', chatMessageEvent);

Hooks.once('ready', () => {
	// console.log("~~~ Last Combatant:", lastCombatant.combatant?.name, { combatant: lastCombatant.combatant }, { spoke: lastCombatant.spoke });
	lastCombatant.combatant = game.combat?.combatant ?? null; // Ensure it matches current
});

Hooks.once('init', () => {
	if (game.modules.get('quench')?.active)
		Hooks.on('quenchReady', () => import('./modules/quench.mjs'));
});
