/**
 * @param {Number} num
 * @param {Number} min
 * @param {Number} max
 * @returns {Number} Number wrapped within the provided range
 */
export function wrapNumber(num, min, max) {
	const range = max - min + 1, value = num - min + 1; // normalize to 1 to range+1
	const t = value % range - 1;
	return (t < 0 ? range + t : t) + min;
}
