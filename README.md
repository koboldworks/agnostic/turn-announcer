# Koboldworks – Turn Announcer

[![Latest Release](https://gitlab.com/koboldworks/agnostic/turn-announcer/-/badges/release.svg)](https://gitlab.com/koboldworks/agnostic/turn-announcer/-/releases)

Announces whose turn it is in chat. Who could it be?

Primarily the purpose of this is to have clear marker in chat log delineating who is acting and when, and making it easier to see who might've missed their turn in case there's issues.

![Chat Message Example](./img/screencaps/chat-message.png)

Also includes optional round cycling message to signal round changes.

  ✅ Recommended for general use

## Configuration

Not available.

## Install

Manifest URL: <https://gitlab.com/koboldworks/agnostic/turn-announcer/-/releases/permalink/latest/downloads/module.json>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).

## Credits

### Translations

|Language|Contributor(s)|
|:---|:---|
|Korean|@drdwing|
