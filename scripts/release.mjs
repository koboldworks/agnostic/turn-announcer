import fs from 'fs';
import { execSync } from 'node:child_process';
import process from 'process';

// TODO: generate version and make sure it does not conflict with existing release tags.

const MANIFEST = './release/module.json',
	CHANGELOG = './CHANGELOG.md';

const args = process.argv.slice(2);

const isHotFix = args.includes('hotfix');
const isBugFix = args.includes('fix') && !isHotFix;
const isMajor = args.includes('major') && !isBugFix;
const isMinor = !isHotFix && !isBugFix && !isMajor;
const release = isMajor ? 'major' : isMinor ? 'minor' : isBugFix ? 'fix' : 'hotfix';

console.log('Release type:', release);

const json = JSON.parse(fs.readFileSync(MANIFEST));
const { version, download } = json;

let [majorVer, minorVer, patchVer, hotfix] = version.split('.').map(n => Number(n) || 0);
console.log('Old version:', [majorVer, minorVer, patchVer, hotfix]);

// Reset version numbers
switch (release) {
	case 'major':
		minorVer = 0;
	case 'minor':
		patchVer = 0;
	case 'fix':
		hotfix = 0;
	case 'hotfix':
		break;
}

// Increment version
switch (release) {
	case 'major': majorVer += 1; break;
	case 'minor': minorVer += 1; break;
	case 'fix': patchVer += 1; break;
	case 'hotfix': hotfix += 1; break;
}

let newVersion = [majorVer, minorVer, patchVer, hotfix].join('.');
newVersion = newVersion.replace(/\.0$/, ''); // Remove hotfix if not present

console.log('New version:', [majorVer, minorVer, patchVer, hotfix], '=', newVersion);

let sameVer = 0;
const newDownload = download.replace(/releases\/(?<version>[\d.]+)\//, function (matched, oldversion) {
	if (oldversion === newVersion) sameVer++;
	return 'releases/' + newVersion + '/';
});

console.log('Old download:', download);
console.log('New download:', newDownload);
json.version = newVersion;
json.download = newDownload;

// changelog update
const chlog = fs.readFileSync(CHANGELOG, { encoding: 'utf8' });
const chlogu = chlog.replace(/^## NEXT$/m, `## ${json.version}`);
if (chlog !== chlogu) {
	console.log('Changelog updated');
	fs.writeFileSync(CHANGELOG, chlogu, 'utf8');
}
else
	console.log('Changelog needs no update');

// Replace version in download string
if (sameVer > 1) {
	console.log('module.json is up to date');
}
else {
	fs.writeFileSync(MANIFEST, JSON.stringify(json, null, '\t'));
	execSync(`npx prettier ${MANIFEST} --write`);
	console.log('module.json updated');
}

execSync(`npx prettier --write ${MANIFEST}`);

// Old version shim
// fs.copyFileSync(MANIFEST, MANIFEST_SHIM);

// git tagging
console.log('\nGenerating release:', json.version);
execSync('git add .');
execSync(`git commit -m "${json.version}"`);
execSync(`git tag -a ${json.version} -m "${json.version}"`);
// execSync(`git tag -f latest`);
console.log('Pushing release');
execSync('git push');
console.log('Pushing release tag');
execSync(`git push origin ${json.version}`);

// done

console.log('\nRelease', json.version, 'is ready!\n');

const releaseManifest = json.download.replace(/\/[\w-]+\.zip$/, '/module.json');

console.log('Manifest:', releaseManifest);
console.log('Download:', json.download);
